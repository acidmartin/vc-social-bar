# vc-social-bar

> Fully customizable social sharing bar component for Facebook, Twitter, Pinterest, LinkedIn, Xing, etc. created by [Martin Ivanov](https://wemakesites.net) and based on [VueJs](https://vuejs.org/) and using Material Design icons pack.

## Demo

https://vc-social-bar.wemakesites.net

## Installation

``
$ npm i vc-social-bar --save
``

## Usage

```
// in main.js, use globally
import VcSocialBar from 'vc-social-bar'
import 'vc-social-bar/dist/lib/vc-social-bar.min.css'
Vue.use(VcSocialBar)

// as a component within another component
import VcSocialBar from 'vc-social-bar'
import 'vc-social-bar/dist/lib/vc-social-bar.min.css'

export default {
  name: 'app',
  components: {
    VcSocialBar
  }
}
```

```
<vc-social-bar
  title="My cool website"
  color="#f2f2f2"
  position="right"
  shadow="0 0 .5em rgba(0, 0, 0, 1)"
  bg-color="rgba(0, 0, 0, .75)"
  :backdrop-blur="5"
  :z-index="1000"
  :font-size="2"
  :popup-width="640"
  :popup-height="480"
  :services="services" />
```

### Adding Services

Below is a sample of a services file you can find in /node_modules/vc-social-bar/src/helpers/vc-social-bar-services.js. You can add as many as you might need.

```
/**
 * Social bar services
 * @module socialBarServices
 */

const socialBarServices = [{
 icon: 'twitter',
 url: (url) => {
   return `https://twitter.com/intent/tweet/?text=&url=${url}`
 }
}, {
 icon: 'facebook',
 url: (url) => {
   return `https://facebook.com/sharer/sharer.php?u=${url}`
 }
}, {
 icon: 'pinterest',
 url: (url) => {
   return `https://pinterest.com/pin/create/button/?url=${url}`
 }
}, {
 icon: 'linkedin',
 url: (url) => {
   return `https://www.linkedin.com/shareArticle?mini=true&url=${url}`
 }
}, {
 icon: 'xing',
 url: (url) => {
   return `https://www.xing.com/app/user?op=share;title=;url=${url}`
 }
}, {
 icon: 'reddit',
 url: (url) => {
   return `https://reddit.com/submit/?url=${url}`
 }
}, {
 icon: 'tumblr',
 url: (url) => {
   return `https://www.tumblr.com/widgets/share/tool?posttype=link&title=&caption=&shareSource=tumblr_share_button&content=${url}&canonicalUrl=${url}`
 }
}, {
 icon: 'whatsapp',
 url: (url, siteTitle) => {
   return `whatsapp://send?text=${encodeURIComponent(siteTitle)} ${url}`
 }
}, {
 icon: 'rss',
 url: () => {
   return `http://feeds.feedburner.com/acidmartin`
 }
}, {
 icon: 'email',
 url: (url, siteTitle) => {
   return `mailto:?subject=${siteTitle}&body=${url}`
 }
}]

export default socialBarServices
```

Then you need to import to your project...

```
import services from './helpers/vc-social-bar-services'

export default {
  name: 'app',
  components: {
    VcSocialBar
  },
  data () {
    return {
      services
    }
  }
}
```

... and set it to the services as shown below:

```
<vc-social-bar :services="services" />
```

## Props

- services (Array, required): required, the social services you will use (explained above), default: []
- popup-width (Number, optional): width of the sharing popup in px, default: 640
- popup-height (Number, optional): height of the sharing popup in px, default: 548
- title (String, optional): sharing text, default: null
- font-size (Number, optional): icons font size in em, default: 1
- color (String, optional): icons color, default: "#f2f2f2"
- position (String, optional): bar position, one of "top", "right", "bottom", "left", default: "left"
- bg-color (String, optional): bar background color , default: "rgba(0, 0, 0, .75)"
- backdrop-blur (Number, optional): bar CSS backdrop-filter blur in px, default: 5
- shadow (String, optional): bar CSS box-shadow, default: "0 0 .5em rgba(0, 0, 0, 1)"
- z-index (Number, optional): component z-index, default: 1000

## Repo

https://bitbucket.org/acidmartin/vc-social-bar/

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Credits

Created by [Martin Ivanov](https://wemakesites.net).